output "gitlab_env_url" {
  value       = join("", ["http://", aws_elb.example.dns_name])
  description = "The domain name of the load balancer"
}
